package com.vti.frontend;

import com.vti.entity.User;
import com.vti.utils.ScannerUtils;

public class ProgramProject {
    public static void main(String[] args) {
        FunctionProject functionProject = new FunctionProject();
        System.out.println("-----------------Đăng nhập-------------------");
        while (true){
            User user = functionProject.login();
            if (user == null){
                System.out.println("Sai thông tin đăng nhập");
            } else {
                System.out.println("Hê lô "+ user.getFullname());
                menu();
            }
        }
    }
    public static void menu(){
        FunctionProject functionProject = new FunctionProject();
        while (true){
            System.out.println("Chọn chức năng");
            System.out.println("1- In ra nhân viên và quản lý của dự án");
            System.out.println("2- In ra tất cả quản lý");
            System.out.println("3- Thoát");
            int choose = ScannerUtils.minMaxNumber(1,3);
            switch (choose){
                case 1:
                    functionProject.findUserByProjectId(); break;
                case 2:
                    functionProject.showManagerProject();break;
                case 3:
                    return;
            }

        }
    }
}
