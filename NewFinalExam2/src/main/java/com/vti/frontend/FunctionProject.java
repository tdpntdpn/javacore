package com.vti.frontend;

import com.vti.backend.Controller.ProjectController;
import com.vti.entity.Project;
import com.vti.entity.User;
import com.vti.utils.ScannerUtils;

import java.util.List;

public class FunctionProject {
    ProjectController projectController = new ProjectController();
    public void findUserByProjectId(){
        System.out.println("Mời nhập vào ID của Project");
        int projectId = ScannerUtils.inputPositiveNumber();
        List<User> userList = projectController.findUserByProjectId(projectId);
        System.out.println("Danh sách user của project " + projectId);
        String leftAlignFormat = "| %-3s | %-7s | %-20s | %-20s |%n";
        System.out.format("+-----+---------+----------------------+----------------------+%n");
        System.out.format("|  id |   role  |       fullname       |         email        |%n");
        System.out.format("+-----+---------+----------------------+----------------------+%n");
        for (User user : userList){
            System.out.format(leftAlignFormat,user.getId(),user.getRole(),user.getFullname(),user.getEmail());
        }
        System.out.format("+-----+---------+----------------------+----------------------+%n");
    }
    public void showManagerProject(){
        List<User> userList = projectController.showManagerProject();
        String leftAlignFormat = "| %-3s | %-9s | %-20s | %-20s |%n";
        System.out.format("+-----+-----------+----------------------+----------------------+%n");
        System.out.format("|  id |   role    |       fullname       |         email        |%n");
        System.out.format("+-----+-----------+----------------------+----------------------+%n");
        for (User user : userList){
            System.out.format(leftAlignFormat,user.getId(),user.getRole(),user.getFullname(),user.getEmail());
        }
        System.out.format("+-----+-----------+----------------------+----------------------+%n");
    }
    public User login(){
        String email = ScannerUtils.inputEmail();
        String password = ScannerUtils.password();
        return  projectController.login(email,password);
    }
}
