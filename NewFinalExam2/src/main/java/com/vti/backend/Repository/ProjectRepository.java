package com.vti.backend.Repository;

import com.vti.entity.Project;
import com.vti.entity.Role;
import com.vti.entity.User;
import com.vti.utils.JdbcUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProjectRepository {
    public static void main(String[] args) {
        ProjectRepository projectRepository = new ProjectRepository();
        projectRepository.showManagerProject();
    }
    public List<User> findUserByProjectId(int projectId){
        Connection connection = JdbcUtils.getConnection();
        String sql = "select * from finalexam2.project p left join `user` u on u.id = p.employeeid or u.id = p.managerid where projectid = ?";
        List<User> userList = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,projectId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setFullname(resultSet.getString("fullname"));
                user.setEmail(resultSet.getString("email"));
                String role = resultSet.getString("role");
                user.setRole(Role.valueOf(role));
                userList.add(user);
            }
        } catch (SQLException e){
            System.out.println(e.getMessage());
        } finally {
            JdbcUtils.closeConnection();
        } return userList;
    }
    public List<User> showManagerProject(){
        Connection connection = JdbcUtils.getConnection();
        String sql = "select * from finalexam2.project p left join `user` u on u.id = p.managerid";
        List<User> userList = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setFullname(resultSet.getString("fullname"));
                user.setEmail(resultSet.getString("email"));
                String role = resultSet.getString("role");
                user.setRole(Role.valueOf(role));
                Project project = new Project();
                project.setProjectName(resultSet.getString("projectname"));
                userList.add(user);
            }
        } catch (SQLException e){
            System.out.println(e.getMessage());
        } finally {
            JdbcUtils.closeConnection();
        } return userList;
    }
    public User login(String email, String password){
        Connection connection = JdbcUtils.getConnection();
        String sql = "Select * from `user` where email = ? and password = ?;";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,email);
            preparedStatement.setString(2,password);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                User user = new User();
                user.setFullname(resultSet.getString("fullname"));
                return user;
            }
        } catch (SQLException e){
            System.out.println(e.getMessage());
        } finally {
            JdbcUtils.closeConnection();
        } throw new RuntimeException();
    }
}
