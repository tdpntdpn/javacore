package com.vti.backend.Controller;

import com.vti.backend.Service.ProjectService;
import com.vti.entity.Project;
import com.vti.entity.User;

import java.util.List;

public class ProjectController {
    ProjectService projectService = new ProjectService();
    public List<User> findUserByProjectId(int projectId) {
        return projectService.findUserByProjectId(projectId);
    }
    public List<User> showManagerProject() {
        return projectService.showManagerProject();
    }
    public User login(String email, String password) {
        return projectService.login(email, password);
    }
}
