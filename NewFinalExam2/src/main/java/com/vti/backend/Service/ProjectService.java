package com.vti.backend.Service;

import com.vti.backend.Repository.ProjectRepository;
import com.vti.entity.Project;
import com.vti.entity.User;

import java.util.List;

public class ProjectService implements IProjectService{
    ProjectRepository projectRepository = new ProjectRepository();
    @Override
    public List<User> findUserByProjectId(int projectId) {
        return projectRepository.findUserByProjectId(projectId);
    }

    @Override
    public List<User> showManagerProject() {
        return projectRepository.showManagerProject();
    }

    @Override
    public User login(String email, String password) {
        return projectRepository.login(email, password);
    }
}
