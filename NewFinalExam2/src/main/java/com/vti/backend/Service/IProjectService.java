package com.vti.backend.Service;

import com.vti.entity.Project;
import com.vti.entity.User;

import java.util.List;

public interface IProjectService {
    List<User> findUserByProjectId(int projectId);
    List<User> showManagerProject();
    User login(String email, String password);
}
