package com.vti.entity;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Generated
public class Employee {
    private int employeeId;
    private String proSkill;
}
