package com.vti.entity;

import lombok.Generated;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Generated
public class User {
    private int id;
    private  Role role;
    private String fullname;
    private String email;
    private String password;
}
