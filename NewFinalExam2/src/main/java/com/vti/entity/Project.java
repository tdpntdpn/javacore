package com.vti.entity;

import lombok.Generated;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Generated

public class Project {
    private int projectId;
    private String projectName;
    private int teamSize;
    private User managerId;
    private User employeeId;
}
