package COM.VTI.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

public class Account {
    private int id;
    private String username;
    private String email;
    private String password;


}
