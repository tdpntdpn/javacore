package COM.VTI.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Setter
@Getter
@ToString
public class User {
    private int userId;
    private Role role;
    private String username;
    private String password;
    private String email;
    private String dateOfBirth;
    private Department10 departmentId;
}
