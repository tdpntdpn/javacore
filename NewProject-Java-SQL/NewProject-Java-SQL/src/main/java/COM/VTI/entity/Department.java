package COM.VTI.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class Department {
    private int id;
    private String departmentName;
}
