package COM.VTI.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class Department10 {
    private int departmentId;
    private String departmentName;
}
