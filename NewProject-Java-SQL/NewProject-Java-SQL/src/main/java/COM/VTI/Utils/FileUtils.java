package COM.VTI.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class FileUtils {
    public static boolean checkFileExists(String pathFile) {
        File file = new File(pathFile);
        if (file.exists()) {
            System.out.println("File co ton tai");
            return true;
        } else {
            System.out.println("File khong ton tai");
            return false;
        }
    }

    public static void createFile(String filename) {
        String pathFile = "Demo/" + filename;
        File file = new File(pathFile);
        try {
            if (file.createNewFile()) {
                System.out.println("Tạo file thành công");
            } else {
                System.out.println("Tạo file không thành công");
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    public static void deleteFile(String pathFile) {
        File file = new File(pathFile);
        if (file.exists()) {
            System.out.println(file.delete() ? "Xoá file thành công" : "Xoá file thất bại");
        } else {
            System.out.println("File không tồn tại");
        }
    }

    public static void getAllFile(String pathFolder) {
        File file = new File(pathFolder);
        if (file.isDirectory()) {
            for (String filename : file.list()) {
                System.out.println(filename);
            }
        } else {
            System.out.println("Đường dẫn không hợp lệ");
        }
    }

    public static void readFile(String pathFile) {
        if (checkFileExists(pathFile)) {
            try {
                FileInputStream fileInputStream = new FileInputStream(pathFile);
                byte[] b = new byte[1024];
                int length = fileInputStream.read(b);
                while (length > -1) {
                    String content = new String(b, 0, length);
                    System.out.println(content);
                    length = fileInputStream.read(b);
                }
                fileInputStream.close();
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        }
    }

    public static void renameFile(String oldName, String newName) {
        // File (or directory) with old name
        File file = new File(oldName);
        // File (or directory) with new name
        File file2 = new File(newName);
        if (file2.exists()) {
            System.out.println("Không được đặt tên đã tồn tại");
        }
        // Rename file (or directory)
        boolean success = file.renameTo(file2);
        System.out.println(success ? "Thay đổi tên thành công" : "Thay đổi tên thất bại");
    }
}