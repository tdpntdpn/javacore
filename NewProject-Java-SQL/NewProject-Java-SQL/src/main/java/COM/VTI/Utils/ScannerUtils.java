package COM.VTI.Utils;

import java.util.Scanner;

public class ScannerUtils {
    public static void main(String[] args) {

    }
    static Scanner scanner = new Scanner(System.in);
    public static String inputString(){
        return scanner.nextLine();
    }
    // tạo 1 method để nhập vào 1 số nguyên dương
    public static int inputPositiveNumber(){
        int positiveNumber = Integer.parseInt(scanner.nextLine());
        while (positiveNumber<=0){
            System.out.println("Mời bạn nhập lại");
            positiveNumber = Integer.parseInt(scanner.nextLine());
        }
        return positiveNumber;
    }
    public static String inputEmail(){
        System.out.println("Mời bạn nhập email");
        String email = scanner.nextLine();
        String s1 = "@";
        while (!email.contains(s1)){
            System.out.println("Chưa đúng định dạng, mời bạn nhập lại");
            email = scanner.nextLine();
        }
        return email ;
    }
    public static int minMaxNumber(int min, int max ){
        try {
            int a = Integer.parseInt(scanner.nextLine());
            while (a < min || a > max) {
                System.out.println("Mời bạn nhập lại");
                a = Integer.parseInt(scanner.nextLine());
            } return a;
        } catch (NumberFormatException e){
            System.err.println("Nhập số đừng nhập chữ");
        }
        return minMaxNumber(min, max);
    }
    public static String password(){
        System.out.println("Nhập vào mật khẩu");
        String password = scanner.nextLine();
        while (password.length()>12 || password.length()<6){
            System.out.println("Chưa đúng, mời nhập lại");
            password = scanner.nextLine();
        }
        return password;
    }

    // thay thế cho minMaxNumber và inputPositiveNumber

    public static int minMaxNumber1(int min, int max){
        int number = 0;
        while (true){
            try {
                number = Integer.parseInt(scanner.nextLine());
                if(number< min || number > max){
                    System.out.println("Số ko đúng định dạng, mời bạn nhập lại");
                    continue;
                }
                break;
            } catch (NumberFormatException ex){
                System.err.println("Nhập vào phải là số, mời nhập lại!");
            }
        }
        return number;
    }
    public static int inputNumberPositive(){
        int number = 0;
        while (true){
            try {
                number = Integer.parseInt(scanner.nextLine());
                if (number <= 0){
                    System.out.println("Số phải lớn hơn 0, mời nhập lại");
                    continue;
                }
                break;
            } catch (NumberFormatException ex){
                System.err.println("Nhập vào phải là số, mời nhập lại!");
            }
        }
        return number;
    }
}




//        try {
//        String sql1 = "";
//        PreparedStatement preparedStatement = connection.prepareStatement(sql1);
//        switch (choose){
//        case 1:
//        sql1 = "update jdbc.account set full_name = ? where account_id = ? ";
//        System.out.println("Nhập username mới");
//        String username = ScannerUtils.inputString();
//        preparedStatement.setString(1,username);
//        preparedStatement.setInt(2,id);
//        break;
//        case 2:
//        sql1 = "update jdbc.account set email = ? where account_id = ? ";
//        System.out.println("Nhập email mới");
//        String email = ScannerUtils.inputEmail();
//        preparedStatement.setString(1,email);
//        preparedStatement.setInt(2,id);
//        break;
//        case 3:
//        sql1 = "update jdbc.account set password = ? where account_id = ? ";
//        System.out.println("Nhập password mới");
//        String password = ScannerUtils.password();
//        preparedStatement.setString(1,password);
//        preparedStatement.setInt(2,id);
//        break;
//        case 4:
//        return;
//        }
//        int resultSet = preparedStatement.executeUpdate();
//        if (resultSet==0){
//        System.out.println("Update thất bại");
//        } else {
//        System.out.println("Update thành công");
//        }
//        }catch (SQLException e){
//        System.out.println(e.getMessage());
//        }