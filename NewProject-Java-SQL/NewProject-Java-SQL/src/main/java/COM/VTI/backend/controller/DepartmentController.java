package COM.VTI.backend.controller;
import COM.VTI.backend.service.DepartmentService;
import COM.VTI.entity.Department;

import java.util.List;

public class DepartmentController {
    DepartmentService departmentService = new DepartmentService();
    public List<Department> getDepartments(){
        return getDepartments();
    }
    public boolean isDepartmentNameExists(String name){
        return departmentService.isDepartmentNameExists(name);
    }
    public void createDepartment(String name){
        departmentService.createDepartment(name);
    }
    public void updateDepartment(int id, String name){
        departmentService.updateDepartment(id,name);
    }
    public boolean checkIdDepartment(int id){
        return departmentService.checkIdDepartment(id);
    }
    public void findDepartmentName(int id){
        departmentService.findDepartmentName(id);
    }
    public void deleteDepartment(int id){
        departmentService.deleteDepartment(id);
    }
    public List<Department> findDepartmentByWord(String name){
        return departmentService.findDepartmentByWord(name);
    }
}
