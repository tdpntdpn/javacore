package COM.VTI.backend;

import COM.VTI.Utils.JdbcUtils;
import COM.VTI.Utils.ScannerUtils;

import java.sql.*;
import java.util.Properties;
import java.util.Scanner;

public class InsertNewPosition {
    public static void main(String[] args) {
        InsertNewPosition insertNewPosition = new InsertNewPosition();
        insertNewPosition.insertPosition();
    }
    public void insertPosition() {
        System.out.println("Nhập tên vị trí mới");
        String position = ScannerUtils.inputString();
        String sql = "insert into`position`(PositionName)" +
                " values(?);";
        Connection connection = JdbcUtils.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,position);
            int resultSet = preparedStatement.executeUpdate();
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}














