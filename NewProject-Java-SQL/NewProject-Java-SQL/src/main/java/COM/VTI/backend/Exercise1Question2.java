package COM.VTI.backend;
import COM.VTI.Utils.JdbcUtils;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class Exercise1Question2 {
    public static void main(String[] args) {
        Connection connection = JdbcUtils.getConnection();
        String query = "SELECT * FROM `position`";
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                int id = resultSet.getInt("PositionID");
                String positionName = resultSet.getString("PositionName");
                System.out.println(id + "-"+ positionName);
            } connection.close();
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
