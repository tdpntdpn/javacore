package COM.VTI.backend.service;

import COM.VTI.entity.Account;

import java.util.List;

public interface IAccountService {
    void createAccount(String username, String email, String password);

    void updateAccount(int id, String oldPassword);

    void deleteAccount(int id);

    List<Account> findByEmail(String email);

    List<Account> getAllAccount();

    boolean login(String email,String password);

    List<Account> getEmailByWord(String word);

}
