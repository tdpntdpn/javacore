package COM.VTI.backend.controller;

import COM.VTI.backend.service.AccountService;
import COM.VTI.entity.Account;

import java.util.List;
// Controller: Dùng để tiếp nhận giá trị đầu vào, và lấy gía trị được trả về ở bên service.
public class AccountController {

    AccountService accountService = new AccountService();
    public void creatAccount(String username,String email,String password){
        accountService.createAccount(username,email,password);
    }
    public void updateAccount(int id, String oldPassword) {
        accountService.updateAccount(id,oldPassword);
    }

    public void deleteAccount(int id) {
        accountService.deleteAccount(id);
    }

    public List<Account> findByEmail(String email) {
        return findByEmail(email);
    }

    public List<Account> getAllAccount() {
        return getAllAccount();
    }

    public boolean login(String email, String password) {
        return accountService.login(email,password);
    }

    public List<Account> getEmailByWord(String word) {
        return accountService.getEmailByWord(word);
    }
}
