package COM.VTI.backend;

import COM.VTI.Utils.ScannerUtils;
import COM.VTI.entity.Role;
import COM.VTI.entity.User;
import COM.VTI.frontend.FunctionUser;

public class ProgramUser {
    public static void main(String[] args) {
        FunctionUser functionUser = new FunctionUser();
        System.out.println("----------- Đăng nhập ----------------");
        while (true){
            User user = functionUser.login();
            if (user == null){
                System.out.println("Tên đăng nhập hoặc mật khẩu không đúng, mời nhập lại");
            } else if (user.getRole()== Role.ADMIN){
                menuAdmin();
            } else {
                menuUser();
            }
        }
    }

    public static void menuUser() {
        FunctionUser functionUser = new FunctionUser();
        while (true) {
            System.out.println("1- Xem tất cả các user");
            System.out.println("2- Tìm user bằng ID");
            System.out.println("3- Tìm user bằng tên hoặc email");
            System.out.println("4- Xem tất cả các phòng ban");
            System.out.println("5- Tìm phòng ban bằng ID");
            System.out.println("6- Tìm phòng ban bằng tên");
            System.out.println("7- Log out");
            System.out.println("8- Thoát");
            int choose = ScannerUtils.minMaxNumber1(1, 8);
            switch (choose) {
                case 1:
                    functionUser.getAllUsers();
                    break;
                case 2:
                    functionUser.findUserByID();
                    break;
                case 3:
                    functionUser.findUser();
                    break;
                case 4:
                    functionUser.getAllDepartments();
                    break;
                case 5:
                    functionUser.findDepartmentByID();
                    break;
                case 6:
                    functionUser.findDepartmentByName();
                    break;
                case 7:
                    break;
                case 8:
                    return;
            } if (choose==7){
                break;
            }
        }
    }
    public static void menuAdmin() {
        FunctionUser functionUser = new FunctionUser();
        while (true) {
            System.out.println("1- Xem tất cả các user");
            System.out.println("2- Xoá user bằng ID");
            System.out.println("3- Thay đổi mật khẩu của 1 user");
            System.out.println("4- Thêm mới 1 user mặc định");
            System.out.println("5- Xem tất cả các phòng ban");
            System.out.println("6- Xoá phòng ban bằng ID");
            System.out.println("7- Đổi tên 1 phòng ban");
            System.out.println("8- Thêm mới 1 phòng ban");
            System.out.println("9- Log out");
            System.out.println("10- Thoát");
            int choose = ScannerUtils.minMaxNumber1(1, 10);
            switch (choose) {
                case 1:
                    functionUser.getAllUsers();
                    break;
                case 2:
                    functionUser.deleteUserByID();
                    break;
                case 3:
                    functionUser.changeUserPassword();
                    break;
                case 4:
                    functionUser.createNewUser();
                    break;
                case 5:
                    functionUser.getAllDepartments();
                    break;
                case 6:
                    functionUser.deleteDepartmentByID();
                    break;
                case 7:
                    functionUser.changeDepartmentName();
                    break;
                case 8:
                    functionUser.createNewDepartment();
                    break;
                case 9:
                    break;
                case 10:
                    return;
            } if (choose==9){
                break;
            }
        }
    }
}
