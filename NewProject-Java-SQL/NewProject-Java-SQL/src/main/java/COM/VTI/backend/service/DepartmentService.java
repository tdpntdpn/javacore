package COM.VTI.backend.service;

import COM.VTI.backend.repository.DepartmentRepository;
import COM.VTI.entity.Department;

import java.util.List;

public class DepartmentService implements IDepartmentService{
    DepartmentRepository departmentRepository =new DepartmentRepository();
    @Override
    public List<Department> getDepartment() {
        departmentRepository.getDepartments();
        return departmentRepository.getDepartments();
    }

    @Override
    public boolean isDepartmentNameExists(String name) {
        return departmentRepository.isDepartmentNameExists(name);
    }

    @Override
    public void findDepartmentName(int id) {
        departmentRepository.findDepartmentName(id);
    }

    @Override
    public void createDepartment(String name) {
        departmentRepository.createDepartment(name);
    }

    @Override
    public void updateDepartment(int id, String name) {
        departmentRepository.updateDepartment(id, name);
    }

    @Override
    public boolean checkIdDepartment(int id) {
        return departmentRepository.checkIdDepartment(id);
    }

    @Override
    public void deleteDepartment(int id) {
        departmentRepository.deleteDepartment(id);
    }

    @Override
    public List<Department> findDepartmentByWord(String name) {
        return departmentRepository.findDepartmentByWord(name);
    }

}
