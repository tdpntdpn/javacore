package COM.VTI.backend;

import COM.VTI.Utils.JdbcUtils;
import COM.VTI.Utils.JdbcUtilsUser;
import COM.VTI.Utils.ScannerUtils;
import COM.VTI.entity.Account;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Example {
    public void createAccount(String username, String email,String password){
        // tạo 1 câu query -> tương ứng với chức năng muốn sử dụng
        String sql = "insert into JDBC.Account (full_name,email,`password`) values(?, ?, ?)";
        // kết nối với database, tạo 1 phiên làm việc
        Connection connection = JdbcUtils.getConnection();
        // tạo statement tương ứng với câu query (có biến truyền vào: PrepareStatement)
        // không có biến truyền vào thì dùng Statement
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,username);
            preparedStatement.setString(2,email);
            preparedStatement.setString(3,password);
            // execute câu query -> lấy kết quả (result)
            int resultSet = preparedStatement.executeUpdate();
            // kiểm tra sự thành công và thông báo
            if (resultSet==0){
                System.out.println("Thêm mới thất bại");
            } else {
                System.out.println("Thêm mới thành công");
            }
            JdbcUtils.closeConnection();
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }
    public List<Account> getAllAccount(){
        List<Account> accountList = new ArrayList<>();
        String sql = "select * from jdbc.account;";
        Connection connection = JdbcUtils.getConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                Account account = new Account();
                int accountId = resultSet.getInt("account_id");
                account.setId(accountId);
                account.setUsername(resultSet.getString("full_name"));
                account.setEmail(resultSet.getString("email"));
                account.setPassword(resultSet.getString("password"));
                accountList.add(account);
            } JdbcUtils.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return accountList;
    }
    public void updateAccount(int id, String oldPassword) {
        Connection connection = JdbcUtils.getConnection();
        System.out.println("Nhập thông tin bạn muốn thay đổi");
        System.out.println("1- Username");
        System.out.println("2- Email");
        System.out.println("3- Password");
        System.out.println("4- Thoát");
        int choose = ScannerUtils.minMaxNumber(1, 4);
        switch (choose) {
            case 1:
                try {
                    String sql = "update jdbc.account set full_name = ? where account_id = ? and password = ? ;";
                    PreparedStatement preparedStatement = connection.prepareStatement(sql);
                    System.out.println("Nhập username mới");
                    String username = ScannerUtils.inputString();
                    preparedStatement.setString(1, username);
                    preparedStatement.setInt(2, id);
                    preparedStatement.setString(3,oldPassword);
                    int resultSet = preparedStatement.executeUpdate();
                    if (resultSet==0){
                        System.out.println("Thay đổi thất bại");
                    } else {
                        System.out.println("Thay đổi thành công");
                    }
                } catch (SQLException e) {
                    System.out.println(e.getMessage());
                }break;
            case 2:
                try {
                    String sql = "update jdbc.account set email = ? where account_id = ? and password = ? ;";
                    PreparedStatement preparedStatement = connection.prepareStatement(sql);
                    System.out.println("Nhập email mới");
                    String email = ScannerUtils.inputEmail();
                    preparedStatement.setString(1, email);
                    preparedStatement.setInt(2, id);
                    preparedStatement.setString(3,oldPassword);
                    int resultSet = preparedStatement.executeUpdate();
                    if (resultSet==0){
                        System.out.println("Thay đổi thất bại");
                    } else {
                        System.out.println("Thay đổi thành công");
                    }
                } catch (SQLException e) {
                    System.out.println(e.getMessage());
                }break;
            case 3:
                try {
                    String sql = "update jdbc.account set password = ? where account_id = ? and password = ? ;";
                    PreparedStatement preparedStatement = connection.prepareStatement(sql);
                    System.out.println("Nhập password mới");
                    String newPassword = ScannerUtils.password();
                    preparedStatement.setString(1, newPassword);
                    preparedStatement.setInt(2, id);
                    preparedStatement.setString(3,oldPassword);
                    int resultSet = preparedStatement.executeUpdate();
                    if (resultSet==0){
                        System.out.println("Thay đổi thất bại");
                    } else {
                        System.out.println("Thay đổi thành công");
                    }
                } catch (SQLException e) {
                    System.out.println(e.getMessage());
                }
                break;
            case 4:
                return;

        } JdbcUtils.closeConnection();

    }
    public void findByEmail(String email){
        Connection connection = JdbcUtils.getConnection();
        String sql = "select * from jdbc.account where email = ? ;";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,email);
            ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    System.out.println("ID: " + resultSet.getInt(1) + ", username: " + resultSet.getString(2) + ", email: " + resultSet.getString(3));
                } else {
                    System.out.println("Email không tồn tại");
                }
            JdbcUtils.closeConnection();
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }
    public void deleteAccount(int choose){
        Connection connection = JdbcUtils.getConnection();
        switch (choose) {
            case 1:
                try {
                    String sql = "delete from jdbc.account where account_id = ? ;";
                    PreparedStatement preparedStatement = connection.prepareStatement(sql);
                    System.out.println("Nhập vào ID");
                    int id = ScannerUtils.inputPositiveNumber();
                    preparedStatement.setInt(1, id);
                    int resultSet = preparedStatement.executeUpdate();
                    if (resultSet==0){
                        System.out.println("Xoá thất bại");
                    } else {
                        System.out.println("Xoá thành công");
                    }
                } catch (SQLException e) {
                    System.out.println(e.getMessage());
                }break;
            case 2:
                try {
                    String sql = "delete from jdbc.account where email = ? ;";
                    PreparedStatement preparedStatement = connection.prepareStatement(sql);
                    System.out.println("Nhập vào email");
                    String email = ScannerUtils.inputEmail();
                    preparedStatement.setString(1, email);
                    int resultSet = preparedStatement.executeUpdate();
                    if (resultSet==0){
                        System.out.println("Xoá thất bại");
                    } else {
                        System.out.println("Xoá thành công");
                    }
                } catch (SQLException e) {
                    System.out.println(e.getMessage());
                }break;
            case 3:
                return;
        } JdbcUtils.closeConnection();
    }
    public boolean login(String email, String password){
        String sql = "select * from jdbc.account where email = ? and password = ?;";
        Connection connection = JdbcUtils.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,email);
            preparedStatement.setString(2,password);
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        }catch (SQLException e){
            System.out.println(e.getMessage());
            return false;
        } finally {
            JdbcUtils.closeConnection();
        }
    }
    public List<Account> getEmailByWord(String word){
        List<Account> accountList = new ArrayList<>();
        String sql = "select * from jdbc.account where email like ? ; ";
        Connection connection = JdbcUtils.getConnection();
        String words = "%"+word+"%";
        try {
            PreparedStatement preparedStatement= connection.prepareStatement(sql);
            preparedStatement.setString(1,words);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Account account = new Account();
                int accountId = resultSet.getInt("account_id");
                account.setId(accountId);
                account.setEmail(resultSet.getString("email"));
                accountList.add(account);
                System.out.println(accountList);
            } JdbcUtils.closeConnection();
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        finally {
            JdbcUtils.closeConnection();
        } return accountList;
    }
}









