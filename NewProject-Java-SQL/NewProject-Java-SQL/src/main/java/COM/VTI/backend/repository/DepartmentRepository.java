package COM.VTI.backend.repository;

import COM.VTI.Utils.JdbcUtils;
import COM.VTI.Utils.ScannerUtils;
import COM.VTI.entity.Department;
import com.mysql.cj.jdbc.ConnectionImpl;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DepartmentRepository {
    public static void main(String[] args) {
        DepartmentRepository departmentRepository = new DepartmentRepository();
        System.out.println(departmentRepository.checkIdDepartment(9));
    }
    public List<Department> getDepartments(){
        List<Department> departmentList = new ArrayList<>();
        String sql = "select * from jdbc.department;";
        Connection connection = JdbcUtils.getConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                Department department = new Department();
                department.setId(resultSet.getInt("id"));
                department.setDepartmentName(resultSet.getString("departmentName"));
                departmentList.add(department);
            }
            System.out.println(departmentList);
        } catch (SQLException e ){
            System.err.println(e.getMessage());
        } finally {
            JdbcUtils.closeConnection();
        }
        return departmentList;
    }
    public boolean isDepartmentNameExists(String name){
        String sql = "select * from jdbc.department where departmentName = ?;";
        Connection connection = JdbcUtils.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,name);
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (SQLException e){
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }
        finally {
            JdbcUtils.closeConnection();
        }
    }
    public void findDepartmentName(int id){
        String sql = "select * from jdbc.department where id = ?;";
        Connection connection = JdbcUtils.getConnection();
        try {
            Department department = new Department();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                department.setId(resultSet.getInt("id"));
                department.setDepartmentName(resultSet.getString("departmentName"));
                System.out.println(department);
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }
    public void createDepartment(String name){
        Connection connection = JdbcUtils.getConnection();
        try {
            String sql = "insert into jdbc.department(departmentName) values ( ? )";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,name);
            int resultSet = preparedStatement.executeUpdate();
            if (resultSet!=0){
                System.out.println("Thêm mới thành công");
            } else {
                System.out.println("Thêm mới thất bại");
            }
        } catch (SQLException e){
            System.out.println(e.getMessage());
        } JdbcUtils.closeConnection();
    }
    public void updateDepartment(int id, String name){
        Connection connection = JdbcUtils.getConnection();
        String sql = "update jdbc.department set departmentName = ? where id = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(2,id);
            preparedStatement.setString(1,name);
            int resultSet = preparedStatement.executeUpdate();
            if (resultSet!=0){
                System.out.println("Cập nhật thành công");
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }
    public boolean checkIdDepartment(int id){
        Connection connection = JdbcUtils.getConnection();
        String sql = "select * from jdbc.department where id = ?";
        try {
            Department department = new Department();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            department.setDepartmentName(resultSet.getString("departmentName"));
            if (resultSet.next()){
                return false;
            } else { return true;
                }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        JdbcUtils.closeConnection();
        return false;
    }
    public void deleteDepartment(int id){
        String sql = "delete from jdbc.department where id = ? ;";
        Connection connection = JdbcUtils.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id);
            preparedStatement.executeUpdate();
            int resultSet = preparedStatement.executeUpdate();
            if (resultSet!=0){
                System.out.println("Đã xoá thành công");
            } else {
                System.out.println("Xoá không thành công");
            }
        } catch (SQLException e ){
            System.out.println(e.getMessage());
        }
        finally {
            JdbcUtils.closeConnection();
        }
    }
    public List<Department> findDepartmentByWord(String name){
        List<Department> departmentList = new ArrayList<>();
        String sql = "select * from jdbc.department where departmentName like ? ;";
        Connection connection = JdbcUtils.getConnection();
        String word = "%"+name+"%";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,word);
            ResultSet resultSet = preparedStatement.executeQuery();
            Department department = new Department();
            department.setId(resultSet.getInt(1));
            department.setDepartmentName(resultSet.getString(2));
            departmentList.add(department);
            if(resultSet.next()){
                System.out.println(departmentList);
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        } finally {
            JdbcUtils.closeConnection();
        }
        return departmentList;
    }
}
