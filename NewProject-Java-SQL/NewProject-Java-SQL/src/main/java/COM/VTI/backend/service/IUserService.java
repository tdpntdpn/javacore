package COM.VTI.backend.service;

import COM.VTI.entity.Department10;
import COM.VTI.entity.User;

import java.util.List;

public interface IUserService {
    User login(String email,String password);

    List<User> getAllUsers();
    User findUserByID(int a);  boolean isUserExistsByID(int a);
    List<User> findUser(String word);
    List<Department10> getAllDepartments();
    Department10 findDepartmentByID(int a);
    List<Department10> findDepartmentByName(String word);

    void deleteUserByID(int a);
    void changeUserPassword(int a, String newPassword);
    void createNewUser(String a, String b); boolean isDepartmentExistsByID(int a);
    void deleteDepartmentByID(int a);
    void changeDepartmentName(int a, String newName);
    void createNewDepartment(String name); boolean isDepartmentExistsByName(String name);

}
