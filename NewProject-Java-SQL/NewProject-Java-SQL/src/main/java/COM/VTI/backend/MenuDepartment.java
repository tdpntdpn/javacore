package COM.VTI.backend;

import COM.VTI.Utils.ScannerUtils;
import COM.VTI.frontend.Function;
import COM.VTI.frontend.FunctionDepartment;

public class MenuDepartment {
    public static void main(String[] args) {
        FunctionDepartment functionDepartment = new FunctionDepartment();
        while (true){
            System.out.println("Mời bạn chọn chức năng");
            System.out.println("1- Thêm mới phòng ban");
            System.out.println("2- Sửa thông tin phòng ban");
            System.out.println("3- Xoá phòng ban theo ID");
            System.out.println("4- Tìm kiếm tên phòng ban theo ID");
            System.out.println("5- Hiển thị tất cả các phòng ban");
            System.out.println("6- Tìm phòng ban bằng từ khoá");
            System.out.println("7- Thoát chương trình");
            int choose = ScannerUtils.minMaxNumber(1,7);
            switch (choose){
                case 1:
                    functionDepartment.createDepartment();
                    break;
                case 2:
                    functionDepartment.updateDepartment();
                    break;
                case 3:
                    functionDepartment.deleteDepartment();
                    break;
                case 4:
                    functionDepartment.findDepartmentName();
                    break;
                case 5:
                    functionDepartment.getDepartments();
                    break;
                case 6:

                    break;
                case 7:
                    return;
            }

        }
    }
}
