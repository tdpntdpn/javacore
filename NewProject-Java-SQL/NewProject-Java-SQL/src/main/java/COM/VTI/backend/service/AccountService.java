package COM.VTI.backend.service;

import COM.VTI.backend.repository.AccountRepository;
import COM.VTI.entity.Account;

import java.util.List;

public class AccountService implements IAccountService {
    AccountRepository accountRepository = new AccountRepository();
    @Override
    public void createAccount(String username, String email, String password) {
        accountRepository.createAccount(username, email, password);
    }

    @Override
    public void updateAccount(int id, String oldPassword) {
        accountRepository.updateAccount(id,oldPassword);
    }

    @Override
    public void deleteAccount(int id) {
        accountRepository.deleteAccount(id);
    }

    @Override
    public List<Account> findByEmail(String email) {
        accountRepository.findByEmail(email);
        return accountRepository.findByEmail(email);
    }

    @Override
    public List<Account> getAllAccount() {
        accountRepository.getAllAccount();
        return accountRepository.getAllAccount();
    }

    @Override
    public boolean login(String email, String password) {
        accountRepository.login(email,password);
        return accountRepository.login(email,password);
    }

    @Override
    public List<Account> getEmailByWord(String word) {
        accountRepository.getEmailByWord(word);
        return accountRepository.getEmailByWord(word);
    }
}
