package COM.VTI.backend;
import COM.VTI.Utils.ScannerUtils;
import COM.VTI.frontend.Function;

public class abc {
    public static void main(String[] args) {
        Function function = new Function();
        while (true){
            System.out.println("Mời bạn chọn chức năng");
            System.out.println("1- Thêm mới account");
            System.out.println("2- Sửa thông tin account");
            System.out.println("3- Xoá account theo ID hoặc email");
            System.out.println("4- Tìm kiếm account theo email");
            System.out.println("5- Hiển thị tất cả account");
            System.out.println("6- Login");
            System.out.println("7- Tìm email bằng từ khoá");
            System.out.println("8- Thoát chương trình");
            int choose = ScannerUtils.minMaxNumber(1,8);
            switch (choose){
                case 1:
                    function.createAccount();
                    break;
                case 2:
                    function.updateAccount();
                    break;
                case 3:
                    function.deleteAccount();
                    break;
                case 4:
                    function.findByEmail();
                    break;
                case 5:
                    function.getAllAccount();
                    break;
                case 6:
                    function.login();
                    break;
                case 7:
                    function.getEmailByWord();
                case 8:
                    return;
            }

        }
    }
}
