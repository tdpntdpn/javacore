package COM.VTI.backend.service;

import COM.VTI.backend.repository.UserRepository;
import COM.VTI.entity.Department10;
import COM.VTI.entity.User;

import java.util.List;

public class UserService implements IUserService{
    UserRepository userRepository = new UserRepository();
    @Override
    public User login(String email, String password) {
    return userRepository.login(email,password);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.getAllUsers();
    }

    @Override
    public User findUserByID(int a) {
    return userRepository.findUserByID(a);
    }

    @Override
    public boolean isUserExistsByID(int a) {
            return userRepository.isUserExistsByID(a);
    }

    @Override
    public List<User> findUser(String word) {
        return userRepository.findUser(word);
    }

    @Override
    public List<Department10> getAllDepartments() {
        return userRepository.getAllDepartments();
    }

    @Override
    public Department10 findDepartmentByID(int a) {
        return userRepository.findDepartmentByID(a);
    }

    @Override
    public List<Department10> findDepartmentByName(String word) {
        return userRepository.findDepartmentByName(word);
    }

    @Override
    public void deleteUserByID(int a) {
        userRepository.deleteUserByID(a);
    }

    @Override
    public void changeUserPassword(int a, String newPassword) {
         userRepository.changeUserPassword(a, newPassword);
    }

    @Override
    public void createNewUser(String a, String b) {
         userRepository.createNewUser(a,b);
    }

    @Override
    public boolean isDepartmentExistsByID(int a) {
        return userRepository.isDepartmentExistsByID(a);
    }

    @Override
    public void deleteDepartmentByID(int a) {
         userRepository.deleteDepartmentByID(a);
    }

    @Override
    public void changeDepartmentName(int a, String newName) {
        userRepository.changeDepartmentName(a, newName);
    }

    @Override
    public void createNewDepartment(String name) {
        userRepository.createNewDepartment(name);
    }

    @Override
    public boolean isDepartmentExistsByName(String name) {
        return userRepository.isDepartmentExistsByName(name);
    }
}
