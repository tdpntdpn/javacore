package COM.VTI.backend.repository;

import COM.VTI.Utils.JdbcUtils;
import COM.VTI.Utils.JdbcUtilsUser;
import COM.VTI.entity.Department;
import COM.VTI.entity.Department10;
import COM.VTI.entity.Role;
import COM.VTI.entity.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserRepository {
    public User login(String email, String password){
        Connection connection = JdbcUtilsUser.getConnection();
        String sql = "select * from assignment10.user where email = ? and password = ? ";
        try {
            User user = new User();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,email);
            preparedStatement.setString(2,password);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                user.setUsername(resultSet.getString("user_name"));
                String role = resultSet.getString("role");
                user.setRole(Role.valueOf(role));
                return user;
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        } finally {
            JdbcUtilsUser.closeConnection();
        } return null;
    }
    public List<User> getAllUsers(){
        List<User> userList = new ArrayList<>();
        Connection connection = JdbcUtilsUser.getConnection();
        String sql = "SELECT * from user u left join department d on u.department_id = d.department_id;";
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                User user = new User();
                user.setUserId(resultSet.getInt("Id"));
                user.setUsername(resultSet.getString("user_name"));
                user.setEmail(resultSet.getString("email"));
                user.setDateOfBirth(resultSet.getString("date_of_birth"));
                String role = resultSet.getString("role");
                user.setRole(Role.valueOf(role));
                Department10 department = new Department10();
                department.setDepartmentName(resultSet.getString(2));
                user.setDepartmentId(department);
                userList.add(user);
            }
        } catch (SQLException e){
            System.out.println(e.getMessage());
        } finally {
            JdbcUtilsUser.closeConnection();
        }
        return userList;
    }
    public User findUserByID(int a){
        Connection connection = JdbcUtilsUser.getConnection();
        String sql = "select * from user where id = ? ;";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,a);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                User user = new User();
                user.setUserId(resultSet.getInt("Id"));
                user.setUsername(resultSet.getString("user_name"));
                user.setEmail(resultSet.getString("email"));
                user.setDateOfBirth(resultSet.getString("date_of_birth"));
                String role = resultSet.getString("role");
                user.setRole(Role.valueOf(role));
                Department10 department = new Department10();
                department.setDepartmentId(resultSet.getInt("department_id"));
                user.setDepartmentId(department);
                return user;
            }
        } catch (SQLException e){
            System.out.println(e.getMessage());
        } finally {
            JdbcUtilsUser.closeConnection();
        } return null;
    }
    public boolean isUserExistsByID(int a){
        Connection connection = JdbcUtilsUser.getConnection();
        String sql = "select * from user where id = ?;";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,a);
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (SQLException e){
            System.out.println(e.getMessage());
        } finally {
            JdbcUtilsUser.closeConnection();
        } throw new RuntimeException();
    }
    public List<User> findUser(String word){
        Connection connection = JdbcUtilsUser.getConnection();
        String sql = "select * from user u left join department d on u.department_id = d.department_id where user_name like ? or email like ? ";
        String words = "%"+word+"%";
        List<User> userList = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,words);
            preparedStatement.setString(2,words);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                User user = new User();
                Department10 department10 = new Department10();
                user.setUserId(resultSet.getInt("id"));
                user.setUsername(resultSet.getString("user_name"));
                String role = resultSet.getString("role");
                user.setRole(Role.valueOf(role));
                user.setEmail(resultSet.getString("email"));
                department10.setDepartmentName(resultSet.getString("department_name"));
                user.setDepartmentId(department10);
                userList.add(user);
            }
        } catch (SQLException e){
            System.out.println(e.getMessage());
        } finally {
            JdbcUtilsUser.closeConnection();
        } return userList;
    }
    public List<Department10> getAllDepartments(){
        List<Department10> department10List = new ArrayList<>();
        Connection connection = JdbcUtilsUser.getConnection();
        String sql = "select * from department; ";
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                Department10 department10 = new Department10();
                department10.setDepartmentId(resultSet.getInt("department_id"));
                department10.setDepartmentName(resultSet.getString("department_name"));
                department10List.add(department10);
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        } finally {
            JdbcUtilsUser.closeConnection();
        }
        return department10List;
    }
    public Department10 findDepartmentByID(int a){
        Connection connection = JdbcUtilsUser.getConnection();
        String sql = "select * from department where department_id = ? ;";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,a);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                Department10 department10 = new Department10();
                department10.setDepartmentId(a);
                department10.setDepartmentName(resultSet.getString("department_name"));
                System.out.println(department10);
                return department10;
            }
        } catch (SQLException e){
            System.out.println(e.getMessage());
        } finally {
            JdbcUtilsUser.closeConnection();
        } throw new RuntimeException();
    }
    public List<Department10> findDepartmentByName(String word){
        Connection connection = JdbcUtilsUser.getConnection();
        String sql = "select * from department where department_name like ?";
        String name = "%"+word+"%";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,name);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Department10> department10List = new ArrayList<>();
            while (resultSet.next()) {
                    Department10 department10 = new Department10();
                    department10.setDepartmentId(resultSet.getInt("department_id"));
                    department10.setDepartmentName(resultSet.getString("department_name"));
                    department10List.add(department10);
            } return department10List;
        } catch (SQLException e){
            System.out.println(e.getMessage());
        } finally {
            JdbcUtilsUser.closeConnection();
        } throw new RuntimeException();
    }
    public void deleteUserByID(int a){
        Connection connection = JdbcUtilsUser.getConnection();
        String sql = "DELETE from user where id = ? ;";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,a);
            int resultSet = preparedStatement.executeUpdate();
            if (resultSet!=0){
                System.out.println("Đã xoá thành công");
            }
        } catch (SQLException e){
            System.out.println(e.getMessage());
        } finally {
            JdbcUtilsUser.closeConnection();
        }
    }
    public void changeUserPassword(int a, String newPassword){
        Connection connection = JdbcUtilsUser.getConnection();
        String sql = "UPDATE user set password = ? where id = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,newPassword);
            preparedStatement.setInt(2,a);
            int resultSet = preparedStatement.executeUpdate();
            if (resultSet!=0){
                System.out.println("Thay đổi thành công");
            } else {
                System.out.println("Thay đổi thất bại");
            }
        } catch (SQLException e){
            System.out.println(e.getMessage());
        } finally {
            JdbcUtilsUser.closeConnection();
        }
    }
    public void createNewUser(String a, String b){
        Connection connection = JdbcUtilsUser.getConnection();
        String sql = "insert into user(role,user_name, password, email) values (?,?,?,?);";
        String password = "123456";
        String role = String.valueOf(Role.USER);
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,role);
            preparedStatement.setString(2,a);
            preparedStatement.setString(3,password);
            preparedStatement.setString(4,b);
            int resultSet = preparedStatement.executeUpdate();
            if (resultSet!=0){
            User user = new User();
            user.setRole(Role.valueOf(role));
            user.setUsername(a);
            user.setPassword(password);
            user.setEmail(b);
                System.out.println(user);
            } else {
                System.out.println("Không thêm được");
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        } finally {
            JdbcUtilsUser.closeConnection();
        }
    }
    public boolean isDepartmentExistsByID(int a){
        Connection connection = JdbcUtilsUser.getConnection();
        String sql = "select * from department where department_id = ?;";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,a);
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (SQLException e){
            System.out.println(e.getMessage());
        } finally {
            JdbcUtilsUser.closeConnection();
        } throw new RuntimeException();
    }
    public void deleteDepartmentByID(int a){
        Connection connection = JdbcUtilsUser.getConnection();
        String sql = "DELETE from department where department_id = ? ;";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,a);
            int resultSet = preparedStatement.executeUpdate();
            if (resultSet!=0){
                System.out.println("Đã xoá thành công");
            }
        } catch (SQLException e){
            System.out.println(e.getMessage());
        } finally {
            JdbcUtilsUser.closeConnection();
        }
    }
    public void changeDepartmentName(int a, String newName){
        Connection connection = JdbcUtilsUser.getConnection();
        String sql = "UPDATE department set department_name = ? where department_id = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,newName);
            preparedStatement.setInt(2,a);
            int resultSet = preparedStatement.executeUpdate();
            if (resultSet!=0){
                System.out.println("Thay đổi thành công");
            } else {
                System.out.println("Thay đổi thất bại");
            }
        } catch (SQLException e){
            System.out.println(e.getMessage());
        } finally {
            JdbcUtilsUser.closeConnection();
        }
    }
    public void createNewDepartment(String name){
        Connection connection = JdbcUtilsUser.getConnection();
        String sql = "insert into department(department_name) values (?);";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,name);
            int resultSet = preparedStatement.executeUpdate();
            if (resultSet!=0){
                System.out.println("Thêm mới thành công");
            } else {
                System.out.println("Thêm mới thất bại");
            }
        } catch (SQLException e){
            System.out.println(e.getMessage());
        } finally {
            JdbcUtilsUser.closeConnection();
        }
    }
    public boolean isDepartmentExistsByName(String name){
        Connection connection = JdbcUtilsUser.getConnection();
        String sql = "select * from department where department_name = ?;";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,name);
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (SQLException e){
            System.out.println(e.getMessage());
        } finally {
            JdbcUtilsUser.closeConnection();
        } throw new RuntimeException();
    }
}
