package COM.VTI.backend.service;

import COM.VTI.entity.Department;

import java.util.List;

public interface IDepartmentService {
    List<Department> getDepartment();
    boolean isDepartmentNameExists(String departmentName);
    void findDepartmentName(int id);
    void createDepartment(String name);
    void updateDepartment(int id, String name);
    boolean checkIdDepartment(int id);
    void deleteDepartment(int id);
    List<Department> findDepartmentByWord(String name);
}
