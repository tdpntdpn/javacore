package COM.VTI.backend.controller;

import COM.VTI.backend.service.UserService;
import COM.VTI.entity.Department10;
import COM.VTI.entity.User;

import java.util.List;

public class UserController {
    UserService userService = new UserService();
    public User login(String email, String password){
        return userService.login(email, password);
    }
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }
    public User findUserByID(int a) {
        return userService.findUserByID(a);
    }
    public boolean isUserExistsByID(int a) {
        return userService.isUserExistsByID(a);}
    public List<User> findUser(String word){
        return userService.findUser(word);
    }
    public Department10 findDepartmentByID(int a){
        return userService.findDepartmentByID(a);
    }
    public List<Department10> getAllDepartments() {
        return userService.getAllDepartments();
    }
    public List<Department10> findDepartmentByName(String word) {
        return userService.findDepartmentByName(word);
    }
    public void deleteUserByID(int a) {
        userService.deleteUserByID(a);
    }
    public void changeUserPassword(int a, String newPassword) {
         userService.changeUserPassword(a, newPassword);
    }
    public void createNewUser(String a, String b) {
         userService.createNewUser(a,b);
    }
    public boolean isDepartmentExistsByID(int a) {
        return userService.isDepartmentExistsByID(a);
    }
    public void deleteDepartmentByID(int a) {
         userService.deleteDepartmentByID(a);
    }
    public void changeDepartmentName(int a, String newName) {
        userService.changeDepartmentName(a, newName);
    }
    public boolean isDepartmentExistsByName(String name){
        return userService.isDepartmentExistsByName(name);
    }
    public void createNewDepartment(String name) {
        userService.createNewDepartment(name);
    }
}
