package COM.VTI.frontend;

import COM.VTI.Utils.JdbcUtils;
import COM.VTI.Utils.ScannerUtils;
import COM.VTI.backend.controller.DepartmentController;
import COM.VTI.entity.Department;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FunctionDepartment {
    DepartmentController departmentController = new DepartmentController();
    public void getDepartments(){
        List<Department> departmentList = departmentController.getDepartments();
        String leftAlignFormat = "| %3s|   %16s |%n";
        System.out.format("+----+------------------------+");
        System.out.format("+ id +   department name      +");
        System.out.format("+----+------------------------+");
        for (Department department: departmentList){
            System.out.format(leftAlignFormat,department.getId(),department.getDepartmentName());
        }
        System.out.format("+----+------------------------+");
    }
    public void isDepartmentNameExists(){
        System.out.println("Nhập vào tên phòng ban");
        String departmentName = ScannerUtils.inputString();
        if (departmentController.isDepartmentNameExists(departmentName)){
            System.out.println("Đã có phòng ban "+departmentName);
        } else {
            System.out.println("Không tồn tại phòng ban");
        }
    }
    public void createDepartment(){
        System.out.println("Nhập vào tên phòng ban");
        String name = ScannerUtils.inputString();
        while ( departmentController.isDepartmentNameExists(name)){
            System.out.println("Đã tồn tại phòng ban");
            name = ScannerUtils.inputString();
        }
        departmentController.createDepartment(name);
    }
    public void updateDepartment(){
        System.out.println("Nhập ID phòng ban");
        int id = ScannerUtils.inputNumberPositive();
        while (departmentController.checkIdDepartment(id)) {
            System.out.println("ID phòng ban chưa tồn tại");
            id = ScannerUtils.inputNumberPositive();
        }
        System.out.println("Nhập tên mới của phòng ban");
        String name = ScannerUtils.inputString();
        while (departmentController.isDepartmentNameExists(name)){
            System.out.println("Đã tồn tại phòng ban");
            name = ScannerUtils.inputString();
        }
        departmentController.updateDepartment(id, name);
    }
    public void findDepartmentName(){
        System.out.println("Nhập vào ID của phòng ban");
        int id = ScannerUtils.inputNumberPositive();
        departmentController.findDepartmentName(id);
    }
    public void deleteDepartment(){
        System.out.println("Nhập ID phòng ban");
        int id = ScannerUtils.inputNumberPositive();
        while (departmentController.checkIdDepartment(id)) {
            System.out.println("ID phòng ban chưa tồn tại");
            id = ScannerUtils.inputNumberPositive();
        }
        departmentController.deleteDepartment(id);
    }
    public void findDepartmentByWord(){
        System.out.println("Nhập ký tự cần tìm");
        String name  = ScannerUtils.inputString();
        List<Department> departmentList = departmentController.findDepartmentByWord(name);
        String leftAlignFormat = "| %-3s| %-15s |%n";
        System.out.format("+----+-------------------+%n");
        System.out.format("| id |   DepartmentName  |%n");
        System.out.format("+----+-------------------+%n");
        for (Department department : departmentList){
            System.out.format(leftAlignFormat, department.getId(), department.getDepartmentName());
        }
        System.out.format("+----+-------------------+%n");
    }
}
