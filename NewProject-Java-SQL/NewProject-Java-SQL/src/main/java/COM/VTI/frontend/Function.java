package COM.VTI.frontend;

import COM.VTI.Utils.ScannerUtils;
import COM.VTI.backend.Example;
import COM.VTI.backend.controller.AccountController;
import COM.VTI.entity.Account;
import java.util.List;

public class Function {
    AccountController accountController = new AccountController();

    public void createAccount() {
        System.out.println("Mời bạn nhập vào username");
        String username = ScannerUtils.inputString();
        String email = ScannerUtils.inputEmail();
        String password = ScannerUtils.password();
        accountController.creatAccount(username, email, password);
    }

    public void updateAccount() {
        System.out.println("Nhập AccountID cần update");
        int id = ScannerUtils.inputPositiveNumber();
        System.out.println("Nhập password cũ");
        String oldPassword = ScannerUtils.password();
        accountController.updateAccount(id, oldPassword);
    }

    public void deleteAccount() {
        System.out.println("Phương thức xoá");
        System.out.println("1- AccountID");
        System.out.println("2- Email");
        System.out.println("3- Huỷ xoá");
        int choose = ScannerUtils.minMaxNumber(1, 3);
        accountController.deleteAccount(choose);
    }

    public void findByEmail() {
        String email = ScannerUtils.inputEmail();
        accountController.findByEmail(email);
    }

    public void getAllAccount(){
        List<Account> accountList = accountController.getAllAccount();
        String leftAlignFormat = "| %-3s| %-15s | %-17s | %-15s |%n";
        System.out.format("+----+-----------------+-------------------+-----------------+%n");
        System.out.format("| id |    fullName     |       Email       |     Passwword   |%n");
        System.out.format("+----+-----------------+-------------------+-----------------+%n");
        for (Account account : accountList){
            System.out.format(leftAlignFormat, account.getId(), account.getUsername(),
                    account.getEmail(), account.getPassword());
        }
        System.out.format("+----+-----------------+-------------------+-----------------+%n");
    }
    public void login(){
        String email = ScannerUtils.inputEmail();
        String password = ScannerUtils.password();
        if (accountController.login(email, password)){
            System.out.println("Đăng nhập thành công");
        } else {
            System.err.println("Đăng nhập thất bại");
        }
    }
    public void getEmailByWord(){
        System.out.println("Ký tự cần tìm");
        String word = ScannerUtils.inputString();
        List<Account> accountList =accountController.getEmailByWord(word);
        String leftAlignFormat = "| %-3s| %-15s | %-17s | %-15s |%n";
        System.out.format("+----+-----------------+-------------------+-----------------+%n");
        System.out.format("| id |    fullName     |       Email       |     Passwword   |%n");
        System.out.format("+----+-----------------+-------------------+-----------------+%n");
        for (Account account : accountList){
            System.out.format(leftAlignFormat, account.getId(), account.getUsername(),
                    account.getEmail(), account.getPassword());
        }
        System.out.format("+----+-----------------+-------------------+-----------------+%n");
    }
}
