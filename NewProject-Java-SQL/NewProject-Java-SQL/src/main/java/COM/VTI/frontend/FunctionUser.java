package COM.VTI.frontend;

import COM.VTI.Utils.ScannerUtils;
import COM.VTI.backend.controller.UserController;
import COM.VTI.entity.Department;
import COM.VTI.entity.Department10;
import COM.VTI.entity.User;

import java.util.ArrayList;
import java.util.List;

public class FunctionUser {
    UserController userController = new UserController();
    public User login(){
        String email = ScannerUtils.inputEmail();
        String password = ScannerUtils.password();
        return userController.login(email,password);
    }
    public void getAllUsers(){
        List<User> userList = userController.getAllUsers();
        String leftAlignFormat = "| %-3s| %-8s | %-15s | %-17s | %-15s | %-15s |%n";
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
        System.out.format("| id |   role   |     username    |        email      | date_of_birth   | department_name |%n");
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
        for (User user: userList) {
            System.out.format(leftAlignFormat, user.getUserId(), user.getRole().name(), user.getUsername(), user.getEmail(),
                    user.getDateOfBirth(), user.getDepartmentId().getDepartmentName());
        }
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
    }
    public void findUserByID(){
        System.out.println("Nhập ID");
        int a = ScannerUtils.inputNumberPositive();
        if (userController.isUserExistsByID(a)){
            User user = userController.findUserByID(a);
            String leftAlignFormat = "| %-3s| %-8s | %-15s | %-17s | %-15s | %-15s |%n";
            System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
            System.out.format("| id |   role   |     username    |        email      | date_of_birth   | department_name |%n");
            System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
                System.out.format(leftAlignFormat, user.getUserId(), user.getRole().name(), user.getUsername(), user.getEmail(),
                        user.getDateOfBirth(), user.getDepartmentId().getDepartmentName());
            System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");;
        } else {
            System.out.println("Không tồn tại user");
        }
    }
    public void findUser() {
        System.out.println("Nhập vào ký tự");
        String word = ScannerUtils.inputString();
        List<User> userList = userController.findUser(word);
        String leftAlignFormat = "| %-3s| %-8s | %-15s | %-17s | %-15s | %-15s |%n";
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
        System.out.format("| id |   role   |     username    |        email      | date_of_birth   | department_name |%n");
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
        for (User user : userList) {
            System.out.format(leftAlignFormat, user.getUserId(), user.getRole().name(), user.getUsername(), user.getEmail(),
                    user.getDateOfBirth(), user.getDepartmentId().getDepartmentName());
            System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
        }
    }
    public void findDepartmentByID(){
        System.out.println("Nhập vào ID phòng ban");
        int a = ScannerUtils.inputNumberPositive();
        if (userController.isUserExistsByID(a)){
            userController.findDepartmentByID(a);
        } else {
            System.out.println("Không tồn tại department có id = "+a);
        }
    }
    public void getAllDepartments(){
        List<Department10> department10List = userController.getAllDepartments();
        String leftAlignFormat = "| %-3s|   %-16s |%n";
        System.out.format("+----+--------------------+%n");
        System.out.format("| id |   department_name  |%n");
        System.out.format("+----+--------------------+%n");
        for (Department10 department10: department10List) {
            System.out.format(leftAlignFormat, department10.getDepartmentId(), department10.getDepartmentName());
        }
        System.out.format("+----+--------------------+%n");
    }
    public void findDepartmentByName() {
        System.out.println("Nhập vào tên chính xác hoặc gần đúng của phòng ban");
        String word = ScannerUtils.inputString();
        List<Department10> department10List = userController.findDepartmentByName(word);
        String leftAlignFormat = "| %-3s|   %-16s |%n";
        System.out.format("+----+--------------------+%n");
        System.out.format("| id |   department_name  |%n");
        System.out.format("+----+--------------------+%n");
        for (Department10 department10: department10List) {
            System.out.format(leftAlignFormat, department10.getDepartmentId(), department10.getDepartmentName());
        }
        System.out.format("+----+--------------------+%n");
    }
    public void deleteUserByID() {
        System.out.println("Nhập vào ID user");
        int a = ScannerUtils.inputNumberPositive();
        if (userController.isUserExistsByID(a)) {
            userController.deleteUserByID(a);
        } else {
            System.out.println("Không có user");
        }
    }
    public void changeUserPassword() {
        System.out.println("Nhập vào ID user");
       int a = ScannerUtils.inputNumberPositive();
       if (userController.isUserExistsByID(a)){
           String newPassword = ScannerUtils.password();
           userController.changeUserPassword(a,newPassword);
       } else {
           System.out.println("Không tồn tại user có id = "+a );
       }
    }
    public void createNewUser(){
            System.out.println("Nhập username");
            String a = ScannerUtils.inputString();
            String b = ScannerUtils.inputEmail();
            userController.createNewUser(a,b);
    }
    public void deleteDepartmentByID() {
        System.out.println("Nhập vào ID phòng ban");
        int a = ScannerUtils.inputNumberPositive();
        if (userController.isDepartmentExistsByID(a)){
            userController.deleteDepartmentByID(a);
        } else {
            System.out.println("Không tồn tại phòng ban đó");
        }
    }
    public void changeDepartmentName() {
        System.out.println("Nhập vào ID phòng ban");
        int a = ScannerUtils.inputNumberPositive();
        if (userController.isDepartmentExistsByID(a)){
            System.out.println("Nhập vào tên mới của phòng ban");
            String newName = ScannerUtils.inputString();
            userController.changeDepartmentName(a, newName);
        } else {
            System.out.println("Không tồn tại phòng ban đó");
        }
    }
    public void createNewDepartment(){
        System.out.println("Nhập tên phòng ban");
        String name = ScannerUtils.inputString();
        if (userController.isDepartmentExistsByName(name)){
            System.out.println("Tên phòng ban đã tồn tại");
        } else {
            userController.createNewDepartment(name);
        }
    }
}
