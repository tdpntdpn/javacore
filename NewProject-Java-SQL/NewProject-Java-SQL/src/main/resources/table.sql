drop Database if exists JDBC;
Use JDBC;
drop table if exists `Account`;
create table `Account` (
                           account_id			int auto_increment primary key,
                           full_name			varchar(50),
                           email				varchar(50) not null,
                           `password`			varchar(50) not null
);
DROP PROCEDURE IF EXISTS checkDepartmentName;

DELIMITER $$
CREATE PROCEDURE checkDepartmentName (IN va_department_name nvarchar(20))
BEGIN
    SELECT * FROM jdbc.Department WHERE departmentName = va_department_name;
END$$
DELIMITER ;

CALL checkDepartmentName('Java');